import firebase from "firebase";

const firebaseConfig = {
  apiKey: "AIzaSyBXF5NQRyM-4up983o-dQfHOSPSsV-WY6g",
  authDomain: "messenger2-2828f.firebaseapp.com",
  projectId: "messenger2-2828f",
  storageBucket: "messenger2-2828f.appspot.com",
  messagingSenderId: "810511595651",
  appId: "1:810511595651:web:f262b91b6618f3d63c83b6"
};

const app = !firebase.apps.length
  ? firebase.initializeApp(firebaseConfig)
  : firebase.app()
  ;

const db = app.firestore();
const auth = app.auth();
const provider = new firebase.auth.GoogleAuthProvider()
const storage = firebase.storage()

export { db, auth, provider, storage }