import { useEffect, useRef } from 'react';
import { useAuthState } from 'react-firebase-hooks/auth'
import { Loading } from '../components/Loading'
import { auth, db } from '../firebase'
import { Howl } from 'howler'
import firebase from 'firebase'
import Login from './login'
import '../styles/globals.scss'

const soundRef = 'https://firebasestorage.googleapis.com/v0/b/messenger2-2828f.appspot.com/o/sound%2Fnotification.mp3?alt=media&token=f49436c3-9e80-4981-a978-a1abde9ddc74'

export default ({ Component, pageProps }) => {
  const [user, loading] = useAuthState(auth)
  const loginRef = useRef()

  console.log(user)

  const playSound = () => {
    const sound = new Howl({
      src: [soundRef],
      html5: true
    })

    sound.play()
  }

  useEffect(() => {
    if (user) {
      db.collection('users').doc(user.uid).set({
        uid: user.uid,
        email: user.email,
        lastSeen: firebase.firestore.FieldValue.serverTimestamp(),
        photoURL: user.photoURL,
      },
        { merge: true }
      )
    }

    const unsubscribe = db.collection('notifications')
      .doc(user?.uid)
      .onSnapshot({
        includeMetadataChanges: true
      }, (snapshot) => {
        if (snapshot?.data()?.count > 0 && loginRef?.current) {
          playSound()
        }
      })

    setTimeout(() => {
      loginRef.current = true
    }, 2000);

    return () => {
      unsubscribe()
    }
  }, [user])

  if (loading) return <Loading />

  if (!user) return <Login />

  return <Component {...pageProps} />
}