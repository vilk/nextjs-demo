import Head from 'next/head'
import { SideBar } from '../components/SideBar'

export default function Home() {
  return (
    <div className="container">
      <Head>
        <title>Create Next Aphsdadasahap</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <SideBar />
    </div >
  )
}
