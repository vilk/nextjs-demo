import Head from 'next/head'
import { useAuthState } from 'react-firebase-hooks/auth'
import { ChatScreen } from '../../components/ChatScreen'
import { SettingBar } from '../../components/SettingBar'
import { SideBar } from '../../components/SideBar'
import { db, auth } from '../../firebase'
import styles from '../../styles/chat.module.scss'
import { getRecipientEmail } from '../../utils/getRecipientEmail'

export default ({ chat, messages }) => {
  const [user] = useAuthState(auth)
  const recipient = getRecipientEmail(chat.users, user)

  return (
    <div className={styles.container}>
      <Head>
        <title>Chat with {recipient}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <SideBar />

      <div className={styles.chatContainer}>
        <ChatScreen chat={chat} messages={messages} recipient={recipient} />
      </div>

      <SettingBar chat={chat} />

    </div>
  )
}

export async function getServerSideProps(context) {
  const ref = db.collection('chats').doc(context.query.id)

  const messagesRes = await ref
    .collection('messages')
    .orderBy('timestamp', 'asc')
    .get();

  const messages = messagesRes.docs
    .map(doc => ({
      id: doc.id,
      ...doc.data(),
    }))
    .map(message => ({
      ...message,
      timestamp: message.timestamp.toDate().getTime(),
    }))

  const chatRes = await ref.get();
  const chat = {
    id: chatRes.id,
    ...chatRes.data()
  }

  return {
    props: {
      messages: messages,
      chat: JSON.parse(JSON.stringify(chat))
    }
  }
}