import { Button } from '@material-ui/core'
import Head from 'next/head'
import styles from '../styles/login.module.scss'
import { auth, provider } from '../firebase'

export default () => {

  const signWithGoogle = () => {
    auth.signInWithPopup(provider).catch(alert)
  }

  return (
    <div className={styles.container}>
      <Head>
        <title>Login</title>
        {/* <link rel="icon" href="/images/icon_messenger.png" /> */}
      </Head>

      <div className={styles.logoContainer} >
        <img className={styles.logo} src='/images/icon_messenger.png' />
        <Button onClick={signWithGoogle} variant='outlined'>Sign in with google</Button>
      </div>
    </div>
  )
}