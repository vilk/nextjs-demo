import { Avatar, IconButton } from '@material-ui/core'
import styles from './styles.module.scss'
import { MoreHoriz, VideoCall, Send, Search as SearchIcon, KeyboardArrowDown } from '@material-ui/icons'
import * as EmailValidator from 'email-validator'
import { auth, db, } from '../../firebase'
import { useAuthState } from 'react-firebase-hooks/auth'
import { useCollection } from 'react-firebase-hooks/firestore'
import { Chat } from '../Chat'
import firebase from "firebase"
import { useState } from 'react'
import { Collapse } from "../Collapse"
import { getRecipientEmail } from '../../utils/getRecipientEmail'
import { setting } from '../../utils/config'

export const SettingBar = ({ chat }) => {
  const [user] = useAuthState(auth)

  const [recipientSnapshot] = useCollection(
    db
      .collection('users')
      .where('email', '==', getRecipientEmail(chat?.users, user))
  )
  const recipient = recipientSnapshot?.docs[0]?.data()

  const Option = ({ data }) => {
    return (
      <div className={styles.optionDetail}>
        {data.icon}
        <span>{data.name}</span>
      </div>
    )
  }

  return (
    <div className={styles.container}>
      <div className={styles.header}>
        <Avatar className={styles.userAvatar} src={recipient?.photoURL} onClick={() => auth.signOut()} />

        <div className={styles.infomation}>
          <span className={styles.title}>{recipient?.email}</span>
        </div>

      </div>

      <div
        className={styles.optionContainer}
      >
        {setting.map(option => (
          <Collapse
            key={option.id}
            className={styles.option}
            title={option.title}
          >
            {
              option?.options?.map(item => <Option data={item} />)
            }
          </Collapse>
        ))}
      </div>
    </div>
  )
}