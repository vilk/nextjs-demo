import { Circle } from 'better-react-spinkit'

export const Loading = () => {
  return (
    <center style={{ height: '100vh', display: 'grid', placeItems: 'center' }}>
      <div>
        <img
          src='/images/icon_messenger.png'
          alt=''
          style={{ marginBottom: 10 }}
          height={200}
          width={350}
        />
        <Circle />
      </div>
    </center>
  )
}
