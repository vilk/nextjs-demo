import { Avatar, IconButton } from '@material-ui/core'
import styles from './styles.module.scss'
import { MoreHoriz, VideoCall, Send, Search as SearchIcon } from '@material-ui/icons'
import * as EmailValidator from 'email-validator'
import { auth, db, } from '../../firebase'
import { useAuthState } from 'react-firebase-hooks/auth'
import { useCollection } from 'react-firebase-hooks/firestore'
import { Chat } from '../Chat'
import firebase from "firebase"
import { useState } from 'react'
import { useRouter } from "next/router"

export const SideBar = () => {
  const [user] = useAuthState(auth)
  const router = useRouter()
  const [choose, setChoose] = useState(router?.query?.id)

  const userChatRef = db.collection('chats').orderBy('lastChat', 'desc')
  const [chatsSnapshot] = useCollection(userChatRef)

  const createChat = () => {
    const input = prompt('Enter email address for user you want to chat with')

    if (!input) return null;

    if (EmailValidator.validate(input) && !chatAlreadyExists(input) && input !== user.email) {
      db.collection('chats').add({
        lastChat: firebase.firestore.FieldValue.serverTimestamp(),
        users: [user.email, input]
      })
    }
  }

  const onChoose = (id) => {
    setChoose(id)
  }

  const chatAlreadyExists = (recipientEmail) => {
    !!chatsSnapshot?.docs.find(chat =>
      chat.data().users?.find(user => user === recipientEmail)?.length > 0
    )
  }

  return (
    <div className={styles.container}>
      <div className={styles.header}>
        <div className={styles.infoContainer}>

          <div className={styles.infomation}>
            <Avatar className={styles.userAvatar} src={user.photoURL} onClick={() => auth.signOut()} />
            <span className={styles.title}>Chat</span>
          </div>

          <div>
            <IconButton>
              <MoreHoriz />
            </IconButton>
            <IconButton>
              <VideoCall />
            </IconButton>
            <IconButton onClick={createChat}>
              <Send />
            </IconButton>
          </div>
        </div>

        <div className={styles.search}>
          <SearchIcon />
          <input className={styles.searchInput} placeholder='Search on messenger' />
        </div>
      </div>

      {chatsSnapshot?.docs.map(chat => {
        if (chat.data().users.includes(user.email)) {
          return <Chat onChoose={onChoose} choose={choose === chat.id} key={chat.id} id={chat.id} users={chat.data().users} />
        }
      })}
    </div>
  )
}