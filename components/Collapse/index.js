import { KeyboardArrowDown, KeyboardArrowUp } from "@material-ui/icons"
import { useState } from "react"
import styles from './styles.module.scss'

export const Collapse = ({ title, children }) => {
  const [show, setShow] = useState(false)

  const handleShow = () => {
    setShow(prev => !prev)
  }

  return (
    <div className={styles.container}>
      <div className={styles.option} onClick={handleShow} >
        <span className={styles.title}>{title}</span>
        {
          show ? <KeyboardArrowUp /> : <KeyboardArrowDown />
        }
      </div>

      {show && children}
    </div>
  )
}