import styles from './styles.module.scss'
import { Avatar, Divider, IconButton } from "@material-ui/core"
import { useAuthState } from 'react-firebase-hooks/auth'
import { useCollection } from 'react-firebase-hooks/firestore'
import { getRecipientEmail } from "../../utils/getRecipientEmail"
import { auth, db, storage } from "../../firebase"
import { useRouter } from "next/router"
import { AddCircle, Mood, More, Phone, Photo, Send, Videocam } from "@material-ui/icons"
import { useEffect, useRef, useState } from "react"
import firebase from "firebase"
import { Messenge } from "../Messenge"
import TimeAgo from 'timeago-react'
import dynamic from "next/dynamic"

const Picker = dynamic(() => import('emoji-picker-react'), { ssr: false })

export const ChatScreen = ({ chat, messages, recipient }) => {
  const router = useRouter();
  const [user] = useAuthState(auth)
  const [input, setInput] = useState('')
  const [showEmoji, setShowEmoji] = useState(false)
  const endMessengeRef = useRef(null)

  const pickerRef = useRef(null)
  const inputFile = useRef(null)

  useEffect(() => {
    scrollToBottom()
  }, [chat])

  useEffect(() => {
    addEventListener('mousedown', handleClickOutSide)

    return () => {
      removeEventListener('mousedown', () => { })
    }
  }, [])

  const [messageSnapshot] = useCollection(
    db
      .collection('chats')
      .doc(router.query.id)
      .collection('messages')
      .orderBy('timestamp', 'asc')
  )
  const [recipientSnapshot] = useCollection(
    db
      .collection('users')
      .where('email', '==', getRecipientEmail(chat.users, user))
  )

  const scrollToBottom = () => {
    endMessengeRef?.current?.scrollIntoView({
      behavior: 'smooth',
      block: 'start',
    })
  }

  const showMessenges = () => {
    scrollToBottom()
    if (messageSnapshot) {
      return messageSnapshot?.docs.map((message) => (
        <Messenge
          key={message.id}
          id={message.id}
          user={message.data().user}
          message={{
            ...message.data(),
            timestamp: message.data().timestamp?.toDate().getTime(),
          }}
        />
      ))
    } else {
      return messages?.map((message) => (
        <Messenge key={message.id} id={message.id} user={message.user} message={message} />
      ))
    }
  }

  const sendMessenge = () => {
    if (input) {
      scrollToBottom()

      db.collection('users').doc(user.uid).update({
        lastSeen: firebase.firestore.FieldValue.serverTimestamp(),
      }, { merge: true })

      db.collection('notifications').doc(recipientSnapshot?.docs[0]?.data()?.uid).set({
        count: firebase.firestore.FieldValue.increment(1)
      }, { merge: true })

      db.collection('chats').doc(router.query.id).collection('messages').add({
        timestamp: firebase.firestore.FieldValue.serverTimestamp(),
        message: input,
        user: user.email,
        photoURL: user.photoURL,
        seen: false
      })

      db.collection('chats')
        .doc(router.query.id)
        .collection('notifications')
        .doc(recipientSnapshot?.docs[0]?.data().email)
        .set({
          count: firebase.firestore.FieldValue.increment(1)
        }, { merge: true })

      db.collection('chats').doc(router.query.id).update({
        lastChat: firebase.firestore.FieldValue.serverTimestamp(),
      })

      setInput('')
    }
  }

  const handleClickOutSide = (e) => {
    if (pickerRef?.current && !pickerRef?.current.contains(e.target)) {
      setShowEmoji(false)
    }
  }

  const onEmojiClick = (event, emoji) => {
    setInput((prev) => prev + emoji.emoji)
    //setShowEmoji(false)
  }

  const onInputFile = () => {
    inputFile?.current?.click()
  }

  const handleChoose = async (e) => {
    const file = e.target.files[0]
    const res = await storage.ref(`images/${file?.lastModified}`).put(file)
    const link = await res.ref.getDownloadURL()

    db.collection('chats').doc(router.query.id).collection('messages').add({
      timestamp: firebase.firestore.FieldValue.serverTimestamp(),
      image: link,
      user: user.email,
      photoURL: user.photoURL,
      seen: false
    }).then(() => {
      e.target.value = null
      scrollToBottom()
    })

    db.collection('chats')
      .doc(router.query.id)
      .collection('notifications')
      .doc(recipientSnapshot?.docs[0]?.data().email)
      .set({
        count: firebase.firestore.FieldValue.increment(1)
      }, { merge: true })

    db.collection('notifications').doc(recipientSnapshot?.docs[0]?.data()?.uid).set({
      count: firebase.firestore.FieldValue.increment(1)
    }, { merge: true })
  }

  return (
    <div className={styles.container}>
      <input type='file' accept='image/*' ref={inputFile} onChange={handleChoose} style={{ display: 'none' }} />

      <div className={styles.header}>
        <div className={styles.infomation}>
          <Avatar
            src={recipientSnapshot?.docs[0]?.data().photoURL}
            className={styles.avatar}
          />
          <div className={styles.nameContainer}>
            <span>{recipient}</span>
            {recipientSnapshot ? (
              <p>
                Last seen:{' '}
                {recipientSnapshot?.docs[0]?.data()?.lastSeen?.toDate()
                  ? <TimeAgo datetime={recipientSnapshot?.docs[0].data()?.lastSeen?.toDate()} />
                  : 'Unavailable'}
              </p>
            ) : <p>Loading last active...</p>
            }
          </div>
        </div>

        <div className={styles.iconGroup}>
          <IconButton>
            <Phone className={styles.icon} />
          </IconButton>

          <IconButton>
            <Videocam className={styles.icon} />
          </IconButton>

          <IconButton>
            <More className={styles.icon} />
          </IconButton>
        </div>
      </div>

      <div className={styles.messagesContainer}>
        {showMessenges()}
        <div style={{ paddingTop: 0 }} ref={endMessengeRef} />
      </div>

      <div className={styles.inputContainer}>
        <div className={styles.iconGroup}>
          <IconButton>
            <AddCircle className={styles.icon} />
          </IconButton>

          <IconButton onClick={onInputFile}>
            <Photo className={styles.icon} />
          </IconButton>

          <IconButton disabled={showEmoji} onClick={() => setShowEmoji(true)}>
            <Mood className={styles.icon} />
          </IconButton>

          {showEmoji
            && <div ref={pickerRef} className={styles.iconPickerContainer}>
              <Picker
                groupVisibility={{}}
                onEmojiClick={onEmojiClick}
              />
            </div>}
        </div>

        <div className={styles.inputWrapper}>
          <input
            value={input}
            onChange={e => setInput(e.target.value)}
            className={styles.input}
            onKeyDown={(e) => {
              if (e.key === 'Enter') {
                sendMessenge()
              }
            }}
          />
        </div>
        <IconButton type='submit' disabled={!input} onClick={sendMessenge}>
          <Send className={styles.icon} />
        </IconButton>
      </div>
    </div>
  )
}
