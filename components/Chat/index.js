import styles from './styles.module.scss'
import { Avatar } from "@material-ui/core"
import { useAuthState } from 'react-firebase-hooks/auth'
import { useCollection } from 'react-firebase-hooks/firestore'
import { getRecipientEmail } from "../../utils/getRecipientEmail"
import { auth, db } from "../../firebase"
import { useRouter } from "next/router"
import cn from 'classnames'
import { useEffect, useState } from 'react'
import TimeAgo from 'timeago-react'

export const Chat = ({ id, users, onChoose, choose }) => {
  const [user] = useAuthState(auth)
  const router = useRouter()
  const [notiCount, setNotiCount] = useState()

  const recipientRef = db.collection('users').where('email', '==', getRecipientEmail(users, user))
  const [recipientSnapshot] = useCollection(recipientRef)

  const recipientEmail = getRecipientEmail(users, user)
  const recipient = recipientSnapshot?.docs[0]?.data();

  const [messageSnapshot] = useCollection(
    db
      .collection('chats')
      .doc(router.query.id)
      .collection('messages')
      .orderBy('timestamp', 'desc')
  )

  const preview = messageSnapshot?.docs[0]?.data()

  useEffect(() => {
    const unsubscribe = db.collection('chats')
      .doc(router.query.id)
      .collection('notifications')
      .doc(user?.email)
      .onSnapshot((snapshot) => {
        setNotiCount(snapshot?.data()?.count)
      })

    return () => {
      unsubscribe()
    }
  }, [])

  const turnOffNotification = () => {
    db.collection('chats')
      .doc(router.query.id)
      .collection('notifications')
      .doc(user?.email)
      .set({
        count: 0
      })
  }

  const enterChat = () => {
    turnOffNotification()
    router.push(`/chat/${id}`)
    onChoose(id)
  }

  console.log(preview)

  return (
    <div
      style={{ backgroundColor: choose ? '#F0F2F5' : '' }}
      className={styles.container}
      onClick={enterChat}
    >
      <Avatar
        style={{ width: 56, height: 56 }}
        src={recipient?.photoURL}
        alt=''
        className={styles.avatar}
      />
      <div className={styles.infoContainer}>
        <span
          className={cn(styles.email, {
            [styles.noti]: notiCount > 0,
          })}
        >
          {recipientEmail}
        </span>
        <span className={styles.preview}>
          {
            preview ?
              preview?.image ?
                (preview?.user === user?.email ?
                  'Bạn' : preview?.user) + ' đã gửi 1 ảnh'
                : (preview?.user === user?.email ? 'Bạn: ' : '') + preview?.message
              : ''
          }

        </span>
        <p className={styles.time}>
          {''} <TimeAgo datetime={preview?.timestamp?.toDate()} />
        </p>
      </div>
      {
        notiCount > 0 && <div className={styles.notiDot} />
      }
    </div>
  )
}
