import { useEffect, useRef, useState } from "react"
import styles from './styles.module.scss'
import { useAuthState } from 'react-firebase-hooks/auth'
import { auth, db } from "../../firebase"
import cn from 'classnames'
import { IconButton } from '@material-ui/core'
import moment from 'moment'
import { Mood } from '@material-ui/icons'
import dynamic from "next/dynamic"
import { useRouter } from "next/router"
import firebase from "firebase"

const Picker = dynamic(() => import('emoji-picker-react'), { ssr: false })

export const Messenge = ({ user, message, id }) => {
  const [userLogin] = useAuthState(auth)
  const router = useRouter();
  const [userLoggedIn] = useAuthState(auth)
  const [showHover, setShowHover] = useState(false)
  const [showEmoji, setShowEmoji] = useState(false)
  const pickerRef = useRef(null)

  const typeOfMessenge = userLoggedIn?.email === user ? 'sender' : 'reciever'

  useEffect(() => {
    addEventListener('mousedown', handleClickOutSide)

    return () => {
      removeEventListener('mousedown', () => { })
    }
  }, [])

  const onEmojiClick = (event, emoji) => {
    db
      .collection('chats')
      .doc(router.query.id)
      .collection('messages')
      .doc(id).update({
        reactIcon: emoji.emoji
      })

    db.collection('users').doc(userLogin.uid).update({
      lastSeen: firebase.firestore.FieldValue.serverTimestamp(),
    }, { merge: true })

    db.collection('chats').doc(router.query.id).update({
      lastChat: firebase.firestore.FieldValue.serverTimestamp(),
    })
    setShowEmoji(false)
    setShowHover(false)
  }

  const removeReactIcon = () => {
    db
      .collection('chats').
      doc(router.query.id).
      collection('messages').
      doc(id).update({
        reactIcon: null
      })

    db.collection('users').doc(userLogin.uid).update({
      lastSeen: firebase.firestore.FieldValue.serverTimestamp(),
    }, { merge: true })

    db.collection('chats').doc(router.query.id).update({
      lastChat: firebase.firestore.FieldValue.serverTimestamp(),
    })
  }

  const handleClickOutSide = (e) => {
    if (pickerRef?.current && !pickerRef?.current.contains(e.target)) {
      setShowHover(false)
      setShowEmoji(false)
    }
  }

  return (
    <>
      {
        <div
          title="haha"
          className={cn(styles.message, {
            [styles.sender]: typeOfMessenge === 'sender',
            [styles.reciever]: typeOfMessenge === 'reciever',
          })}
          style={
            message.image ?
              {
                backgroundColor: 'white',
                //border: '1px solid whitesmoke',
                padding: 0,
              }
              : {}
          }
          onMouseEnter={() => setShowHover(true)}
          onMouseLeave={() => {
            if (!showEmoji) {
              setShowHover(false)
            }
          }}
        >

          {showHover &&
            <div className={cn(styles.iconButton, {
              [styles.senderIcon]: typeOfMessenge === 'sender',
              [styles.recieverIcon]: typeOfMessenge === 'reciever',
            })}
            >
              <div style={{ position: 'relative' }}>
                {
                  showEmoji &&
                  <div style={{ right: typeOfMessenge === 'sender' ? 0 : -310 }} ref={pickerRef} className={styles.iconPickerContainer}>
                    <Picker onEmojiClick={onEmojiClick} />
                  </div>
                }
              </div>
              <IconButton onClick={() => setShowEmoji(true)}>
                <Mood className={styles.icon} />
              </IconButton>
            </div>
          }
          {
            message.image ?
              <img
                src={message.image}
                className={styles.imageMessage}
              />
              : <p>
                {message.message}
                <span className={styles.timestamp}>
                  {message.timestamp ? moment(message.timestamp).format('LT') : '...'}
                </span>
              </p>
          }
          <div onClick={removeReactIcon} className={styles.reactIcon}>
            <span style={{ fontSize: 14 }}>
              {message.reactIcon}
            </span>
          </div>
        </div>
      }
    </>
  )
}
