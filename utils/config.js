import { Album, ThumbUp, SortByAlpha, Search, Collections, Note, Link, Notifications, DesktopAccessDisabledOutlined, Lock, Report } from "@material-ui/icons";

export const setting = [
    {
        id: 'setting-1',
        title: 'Tùy chỉnh đoạn chat',
        options: [
            {
                id: 'option-1',
                name: 'Đổi chủ đề',
                icon: <Album />
            },
            {
                id: 'option-2',
                name: 'Thay đổi biểu tượng cảm xúc',
                icon: <ThumbUp />
            },
            {
                id: 'option-3',
                name: 'Chỉnh sửa biệt danh',
                icon: <SortByAlpha />
            },
            {
                id: 'option-4',
                name: 'Tìm kiếm trong cuộc trò chuyện',
                icon: <Search />
            },
        ],
    },
    {
        id: 'setting-2',
        title: 'File phương tiện, file và liên kết',
        options: [
            {
                id: 'option-1',
                name: 'File phương tiện',
                icon: <Collections />
            },
            {
                id: 'option-2',
                name: 'File',
                icon: <Note />
            },
            {
                id: 'option-3',
                name: 'Liên kết',
                icon: <Link />
            },
        ],
    },
    {
        id: 'setting-3',
        title: 'Quyền riêng tư & hỗ trợ',
        options: [
            {
                id: 'option-1',
                name: 'Tắt thông báo',
                icon: <Notifications />
            },
            {
                id: 'option-2',
                name: 'Bỏ qua tin nhắn',
                icon: <DesktopAccessDisabledOutlined />
            },
            {
                id: 'option-3',
                name: 'Chặn',
                icon: <Lock />
            },
            {
                id: 'option-4',
                name: 'Báo cáo',
                icon: <Report />
            },
        ],
    },
]